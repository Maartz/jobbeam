defmodule Jobbeam do
  alias Jobbeam.{JobRunner, JobSupervisor}

  def start_job(args) do
    if Enum.count(running_imports()) >= 5 do
      {:error, :import_threshold_reached}
    else
      DynamicSupervisor.start_child(JobRunner, {JobSupervisor, args})
    end
  end

  def running_imports() do
    # weird Erlang syntax for matching
    # see Elixir's Registry.select/2
    match_all = {:"$1", :"$2", :"$3"}
    guards = [{:==, :"$3", "import"}]
    map_result = [%{id: :"$1", pid: :"$2", type: :"$3"}]
    Registry.select(Jobbeam.JobRegistry, [{match_all, guards, map_result}])
  end
end
